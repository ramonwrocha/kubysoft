// BLANKCOMPONENT  *mycomponent. 

// BEGIN SCOPE 
// self executing function for jquery reference. That prevents the $ is allways jquery component. 
// W = window D= document undefined = nothing prevent undefined = true
(function ($, W, D, undefined) {

	/* ---------------------------------------------------------
	// FUNCTION 
	The function has 4components 
	cMain for main control,
	serverCall for call server to /node 
	data for data manipulation and connect to server. 
	view for view UI  
	
	 ------------------------------------------------------------ */
	var BlankComponent = function () {
		var name = "jthn";
		
		var cMain = {
			// default values *defaults
			defaults: {
				name: 'Default Name',

			}, // end defaults 

			// INIT *init componentn  
			init: function (options, elem) {
				// INI OBJECTS  
				var self = cMain;
				// console.log("FN init"); console.log(this); console.log(cMain); 

				// INI HTML. 
				self.MainDiv = $(elem);

				// INI OPTIONS 
				self.options = $.extend({}, self.defaults, options);

				// // INI DATA AND VIEW 
				self.iniHtml();

				// // CALL MAIN
				// self.Main();

				// self.CallServer({success: self.testReturn},{app:"MarcTest003",option:"1",value1:'marc',DBToken:'Prog001'})

			},// end init

			// *INI *HTML 
			iniHtml: function () {
				var self = this;
				var div = '';
				var arr = self.options.elementos || [];

				for (const obj of arr) {
					var strjson = JSON.stringify(obj);
					strjson = strjson.split('"').join('');

					var txt = `<h2>${obj.value} - ${obj.title}</h2>`;
					var btn = `<button class="btn btn-primary btn-block">${txt}</button>`
					
					div += `<div class="col-md-3" onclick="alert('${strjson}')">${btn}</div>`;
				}

				self.MainDiv.html(div);

			}, // END HTML 

			// ******************************  FUNCTIONS FROM HERE !!!!
			Main: function () {
				var self = this;
				console.log("fn Main", self);
			},

		}// END COMPONENT 

		return cMain

	}; // function 

	// ---------------------------------------------------------
	// JQUERY DECLARED COMPONENT  
	// ------------------------------------------------------------
	$.fn.yourcomponentHere = function (options) {
		
	}

	$.fn.kContadores = function (options) {
		
	}

	$.fn.BlankComponent = BlankComponent;	

})(jQuery, window, document);
// END SCOPE MYCOMPONENT


