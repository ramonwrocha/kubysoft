$(document).ready(function () {
	var aElementos =
		[{
			value: 2, title: "Ahora", id: "h"			
		},
		{
			value: 7, title: "Hoy", id: "h"
		},
		{
			value: 2, title: "Esta semana", id: "h"
		},
		{
			value: 60, title: "Este mes", id: "h"
		}];

	var div = '#maintreball';
	var hMain = $(div);
	var options = {
		elementos: aElementos, selectable: true, multiselect: true,
		onSelect: function (data) {
			console.log(data)
		}
	}

	var bc = hMain.BlankComponent();

	bc.init(options, div)

});

Array.prototype.GetByAttr = function (obj) {
	if (!obj) { return }
	var Matches = [], vAttributes = 0;

	// change atributes to lowercase 

	for (property in obj) {
		// console.log(property + ' ' + obj[property] + ' ' + isNaN(obj[property]) );
		if (isNaN(obj[property])) { obj[property] = obj[property].toLowerCase(); }
		vAttributes = vAttributes + 1;
	}

	// var con = obj[obj.key].toLowerCase();
	var vMatch = 0;
	for (var i = 0; i < this.length; i++) {
		vMatch = 0;
		for (property in obj) {
			if (obj[property].toLowerCase() == this[i][property].toLowerCase()) { vMatch = vMatch + 1 }
		}

		if (vMatch == vAttributes) {
			Matches.push(this[i])
		}
	}// next 
	return Matches;

}; // end GetByName 
